package ca.ulaval.glo3004.threads.lumieres;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

public class LumieresPietonsBarrieres {
	public static HashMap<String, Boolean> etatdesLumiere = new HashMap<>();

	static class LumiereSud extends Thread {

		private CyclicBarrier barriere;

		public Semaphore sem;
		public boolean estAllumee;
		public LinkedList<VoitureEnT> voitures;
		public int nombreVoiture;

		public LumiereSud(CyclicBarrier barriere,Semaphore semaphore,int nombreVoiture) {
			super("lumiere sud");
			this.barriere = barriere;
			this.sem = semaphore;
			this.nombreVoiture = nombreVoiture;
		}

		@Override
		public void run() {
			try {
				Random random = new Random();

				sudPasseAuVert();

				Thread.sleep(random.nextInt(20000-2000)+2000);

				sudPasseAuRouge();

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		private synchronized void sudPasseAuRouge() throws InterruptedException {

			System.out.println("La lumi�re Sud est rouge");
			etatdesLumiere.put("sud", false);

			for (VoitureEnT voiture : voitures){
				voiture.exit  = true;
			}
		}

		private synchronized void sudPasseAuVert() {

			System.out.println("La lumi�re Sud est verte");
			etatdesLumiere.put("sud", true);
			for (int i = 0; i<= nombreVoiture; i++){
				VoitureEnT voiture = new VoitureEnT("est",false);
				voiture.start();
				voitures.add(voiture);
			}
		}
	}

	static class LumiereEst extends Thread {

		private CyclicBarrier barriere;
		public Semaphore sem;

		public LumiereEst(CyclicBarrier barriere,Semaphore semaphore) {
			super("lumiere est");
			this.barriere = barriere;
			this.sem = semaphore;
		}

		@Override
		public void run() {
			try {
				Random random = new Random();
				while (random.nextInt(100) != 42) {
					//Devient vert
					//Sleep sur la lumiere sud 
				}

				System.out.println("La lumi�re est est verte");
				etatdesLumiere.put("est", true);

				//Attend pendant x secondes
				this.barriere.await();
				
				//Wakeup le tread pour les voiture 
				//LECTURE ATOMIQUE sur l'�tat de la lumi�re ouest pour 
				//voir si la voiture peut tourner � gauche 
				
				//Devient rouge 
				//MAYBE SEMAPHORE / LOCK ICI 
				System.out.println("La lumi�re Sud est rouge");
				
				//LECTURE ATOMIQUE sur l'�tat de la lumi�re ouest pour
				//voir si il faut wakeup la lumi�re sud 
				
			} catch (InterruptedException | BrokenBarrierException e) {

			}
		}
	}

	//parail comme est 
	static class LumiereOuest extends Thread {

		private CyclicBarrier barriere;
		public Semaphore sem;

		public LumiereOuest(CyclicBarrier barriere,Semaphore semaphore) {
			super("lumière ouest");
			this.barriere = barriere;
			this.sem = semaphore;
		}

		@Override
		public void run() {
			try {
				Random random = new Random();
				while (random.nextInt(100) != 42) {
					Thread.sleep(1000);
				}

				System.out.println("Un piéton à la lumière ouest");
				etatdesLumiere.put("ouest", true);
				this.barriere.await();
			} catch (InterruptedException | BrokenBarrierException e) {

			}
		}
	}

	
	//###################
	//Fonction voiture
	//###################
	static class VoitureEnT extends Thread {

		String provenance;
		boolean lumiereEnFaceAllumee;
		boolean exit = false;

		public VoitureEnT(String provenance, boolean lumiereEnFaceAllumee) {
			this.provenance = provenance;
			this.lumiereEnFaceAllumee = lumiereEnFaceAllumee;
		}

		@Override
		public void run() {

			Random random = new Random();

			while (!exit) {
				int seed = random.nextInt(3);
				if (provenance.equalsIgnoreCase("sud")) {
					switch (seed) {
						case 1:
							System.out.println("Une voiture du sud tourne � gauche");
							break;
						case 2:
							System.out.println("Une voiture du sud tourne � droite");
							break;
						case 3:
							if(lumiereEnFaceAllumee){
								System.out.println("Une voiture du sud continue tout droit");
							}
							break;
					}

				}
				else if (provenance.equalsIgnoreCase("est")) {

					switch (seed) {
						case 1:
							System.out.println("Une voiture du est continue tout droit");
							break;
						case 2:
							System.out.println("Une voiture du est tourne � droite");
							break;
						case 3:
							if(!lumiereEnFaceAllumee){
								System.out.println("Une voiture du est tourne � gauche");
							}
							break;
					}
				}
				else if (provenance.equalsIgnoreCase("ouest")) {

					switch (seed) {
						case 1:
							System.out.println("Une voiture du ouest continue tout droit");
							break;
						case 2:
							System.out.println("Une voiture du ouest tourne � droite");
							break;
					}
				}
			}
		}
	}
	
	//###################
	//Fonction Pieton
	//###################
	static class Pieton extends Thread {

		boolean exit = false;

		@Override
		public void run() {
			Random random = new Random();
			while (!exit) {
				try {
					System.out.println("Pi�tons traversent");
					Thread.sleep(random.nextInt(2000));
				}
				catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

		}
	}
	
	//###################
	//Main 
	//###################
	public static void main(String[] args) throws InterruptedException {
		Runnable afficheEtat = () -> System.out.println("Sud: " + etatdesLumiere.get("sud") + ", est: " + etatdesLumiere.get("est")
				+ ", ouest: " + etatdesLumiere.get("ouest") + ", nord: " + etatdesLumiere.get("nord"));

		Random random = new Random();
		CyclicBarrier barrier = new CyclicBarrier(4, afficheEtat);
		Semaphore sem = new Semaphore(1);
		Semaphore creation = new Semaphore(1);
		System.out.println("Combien de voiture voulez vous ?");
		int nombreVoiture = Integer.parseInt(System.console().readLine());



		while (true) {
			int seed = random.nextInt(5);

			creation.acquire();
			switch (seed) {
				case 1:
					new LumiereSud(barrier,sem,nombreVoiture).start();
					creation.release();
					//destroy thread
					break;
				case 2:
					new LumiereEst(barrier,sem).start();
					creation.release();
					//destroy thread
					break;
				case 3:
					new LumiereOuest(barrier,sem).start();
					creation.release();
					//destroy thread
					break;
				case 4:
					new LumiereEst(barrier,sem).start();
					new LumiereOuest(barrier,sem).start();
					creation.release();
					//destroy thread
					break;
				case 5:
					Pieton pieton = new Pieton();
					pieton.start();

					Thread.sleep(random.nextInt(20000-2000) +2000);
					pieton.exit = true;
					creation.release();
					break;
				default:
					creation.release();
					break;
			}
		}
	}
}
